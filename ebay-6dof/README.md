# แขนกลจากจีน
สั่งซื้อแขนกลจากจีนราคา 4500บ.(รวมภาษี) ไม่มีส่วนขับมอเตอร์ จึงซื้อ Arduino UNO R3(อาจจะไม่แท้) และ Sensor Shield Expansion Board Shield For Arduino UNO R3 V5.0 เป็นตัวขับมอเตอร์ ใช้ [StandardFirmataEEPROM](https://github.com/firmata/arduino#firmata-client-libraries) และ Raspberry PI3 อุปกรณ์ส่วนหนึ่งไม่ได้ซื้อใหม่เช่น Arduino , RPi เพื่อให้ควบคุมพอร์ต Arduino ผ่าน USB ควบคุมแขนกลผ่านทาง Raspbery Pi ต่อด้วย USB รูปด้านล่างจะช่วยให้เห็นภาพ

pin8(90:0)(up:down) <br>
pin10(145:90)(up:down)<br>
pin11(160:45) (far:near)<br>
pin12(0:180) pan <br>

![6dof1](IMG_3934.JPG)
![6dof2](IMG_3936.JPG)
![6dof3](IMG_4009.JPG)
![6dof3](IMG_4025.JPG)
