# Pin
Arduino +5V to VCC
Arduino GND to GND
Arduino analog input pin A4 to SDA
Arduino analog input pin A5 to SCL

# GY-512
เป็น Accelerometers และ Gyroscope ในที่นี้ใช้ Gyroscope อย่างเดียว โค๊ด MPU6050-servo.ino อ่านค่าจาก Gyroscope แล้วใช้การประมาณค่าผ่าน Kalman filter เลือกแกน `kalAngleY` นำค่าที่ได้ไปควบคุมมอเตอร์
จากรูปติดตั้ง GY-512 บนแกนแขนกล มีแนวแกน 'Y' แนวราบให้ค่า `gyroY = 0` ซึ่งค่า `gyroY` นี่จะถูกแปลงผ่าน ฟังก์ชัน Kalman filter เป็น `kalAngleY`  คำสั่งควบคุมมอเตอร์ที่เพิ่มจากโค้ดต้นฉบับมีดังนี้

```c
if(abs(kalAngleY)>20) {
   posY=posY-(0.9*sgn(kalAngleY));
}else if(abs(kalAngleY)>10) {
   posY=posY-(0.4*sgn(kalAngleY));
}else{
   posY=posY-(0.05*sgn(kalAngleY));
}

if(posY>140) posY=140;
if(posY<0) posY=0;
Serial.print(gyroY); Serial.print("\t");
Serial.print(kalAngleY); Serial.print("\t");
Serial.print(posY);Serial.print("\t");
Serial.print("\r\n");
myservo.write(posY);
delay(10);
```

ลดการส่ายของมอเตอร์(Ping-Pong)ด้วยวิธีกำหนดช่วงที่ยอมรับได้ จากตัวอย่างกำหนดให้ช่วง `-1 < kalAngleY <1` เป็นช่วงที่ยอมรับได้ หรือเปิดโอกาสให้ผิดพลาดได้ 2องศาดีกรี
เมื่อ gyroY อยู่ตำแหน่งนอกเหนือ `-1 < kalAngleY <1` ให้ปรับครั้งละ 0.05องศาดีกรี ไปจนกระทั่ง kalAngleY อยู่ในช่วงที่กำหนด

<a href="http://www.youtube.com/watch?feature=player_embedded&v=93HGBVozm_M" target="_blank"><img src="http://img.youtube.com/vi/93HGBVozm_M/0.jpg" alt="Demo" width="240" height="180" border="10" /></a>
