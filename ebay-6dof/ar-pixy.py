#!/usr/bin/env python
#
# begin license header
#
# This file is part of Pixy CMUcam5 or "Pixy" for short
#
# All Pixy source code is provided under the terms of the
# GNU General Public License v2 (http://www.gnu.org/licenses/gpl-2.0.html).
# Those wishing to use Pixy source code, software and/or
# technologies under different licensing terms should contact us at
# cmucam@cs.cmu.edu. Such licensing terms are available for
# all portions of the Pixy codebase presented here.
#
# end license header
#

# Pixy Tracking Demo - Python Version #

import sys,traceback
import signal
import random
from pixy import *
from ctypes import *
from scipy.interpolate import interp1d
import pyuarm
import os, struct, array
from fcntl import ioctl
from pyfirmata import Arduino, util
import pygame
import sys, traceback
from pykalman import KalmanFilter
from subprocess import call

PIXY_MIN_X             =    0
PIXY_MAX_X             =  319
PIXY_MIN_Y             =    0
PIXY_MAX_Y             =  199

PIXY_X_CENTER          =  ((PIXY_MAX_X-PIXY_MIN_X) / 2)
PIXY_Y_CENTER          =  ((PIXY_MAX_Y-PIXY_MIN_Y) / 2)
PIXY_RCS_MIN_POS       =    0
PIXY_RCS_MAX_POS       = 1000
PIXY_RCS_CENTER_POS    =  ((PIXY_RCS_MAX_POS-PIXY_RCS_MIN_POS) / 2)

PIXY_RCS_PAN_CHANNEL   =    0
PIXY_RCS_TILT_CHANNEL  =    1

PAN_PROPORTIONAL_GAIN  =  400
PAN_DERIVATIVE_GAIN    =  300
TILT_PROPORTIONAL_GAIN =  500
TILT_DERIVATIVE_GAIN   =  400

BLOCK_BUFFER_SIZE      =    1

board = Arduino('/dev/ttyAR')
clock = pygame.time.Clock()
it = util.Iterator(board)
it.start()
board.analog[0].enable_reporting()
board.analog[1].enable_reporting()
board.analog[2].enable_reporting()
board.analog[3].enable_reporting()

pin4 = board.get_pin('d:4:o')
pin6 = board.get_pin('d:6:o')
#pin7 = board.get_pin('d:7:s')
pin8 = board.get_pin('d:8:s')
pin9 = board.get_pin('d:9:s')
pin10 = board.get_pin('d:10:s')
pin11 = board.get_pin('d:11:s')
pin12 = board.get_pin('d:12:s')
pin13 = board.get_pin('d:13:s')

pin14 = board.get_pin('a:0:o')
pin15 = board.get_pin('a:1:o')
pin16 = board.get_pin('a:2:o')
pin17 = board.get_pin('a:3:o')

pin_tilt=pin8

base_state=1
base2_state=1
base4_state=0
slowrate=1
tilt_state=0

global current_tilt
global current_based
global current_end
global current_z
global current_rz

current_tilt=60
current_based=90
current_end=90
current_z=60
current_rz=145

pin8.write(current_tilt)
pin9.write(current_end)
pin10.write(current_rz)
pin11.write(current_z)
pin12.write(current_based)

# Globals #

run_flag = True


class Blocks (Structure):
  _fields_ = [ ("type", c_uint),
               ("signature", c_uint),
               ("x", c_uint),
               ("y", c_uint),
               ("width", c_uint),
               ("height", c_uint),
               ("angle", c_uint) ]

class Gimbal ():
  _fields_ = [ ("position", c_uint),
               ("first_update", bool),
               ("previous_error", c_uint),
               ("proportional_gain", c_uint),
               ("derivative_gain", c_uint) ]

  def __init__(self, start_position, proportional_gain, derivative_gain):
    self.position          = start_position
    self.proportional_gain = proportional_gain
    self.derivative_gain   = derivative_gain
    self.previous_error    = 0
    self.first_update      = True

  def update(self, error):
    if self.first_update == False:
      error_delta = error - self.previous_error
      P_gain      = self.proportional_gain;
      D_gain      = self.derivative_gain;

      # Using the proportional and derivative gain for the gimbal #
      # calculate the change to the position                      #
      velocity = (error * P_gain + error_delta * D_gain) / 1024;

      self.position += velocity;

      if self.position > PIXY_RCS_MAX_POS:
        self.position = PIXY_RCS_MAX_POS
      elif self.position < PIXY_RCS_MIN_POS:
        self.position = PIXY_RCS_MIN_POS
    else:
      self.first_update = False

    self.previous_error = error

def handle_SIGINT(signal, frame):
  global run_flag
  run_flag = False

def main():
  global run_flag
  global current_tilt
  global current_rz
  global current_z
  global current_based

  print '+ Pixy Tracking Demo Started +'

  # Initialize Pixy Interpreter thread #
  pixy_init_status = pixy_init()

  if pixy_init_status != 0:
    print 'Error: pixy_init() [%d] ' % pixy_init_status
    pixy_error(pixy_init_status)
    return

  #  Initialize Gimbals #
  pan_gimbal  = Gimbal(PIXY_RCS_CENTER_POS, PAN_PROPORTIONAL_GAIN, PAN_DERIVATIVE_GAIN)
  tilt_gimbal = Gimbal(PIXY_RCS_CENTER_POS, TILT_PROPORTIONAL_GAIN, TILT_DERIVATIVE_GAIN)

  # Initialize block #
  block       = Block()
  frame_index = 0

  signal.signal(signal.SIGINT, handle_SIGINT)

  # Run until we receive the INTERRUPT signal #
  swap=1
  while run_flag:

    # Do nothing until a new block is available #
    while not pixy_blocks_are_new() and run_flag:
      pass

    # Grab a block #
    count = pixy_get_blocks(BLOCK_BUFFER_SIZE, block)

    # Was there an error? #
    if count < 0:
      print 'Error: pixy_get_blocks() [%d] ' % count
      pixy_error(count)
      board.exit()
      sys.exit(1)

    if count > 0:
      # We found a block #

      # Calculate the difference between Pixy's center of focus #
      # and the target.                                         #
      pan_error  = PIXY_X_CENTER - block.x
      tilt_error = block.y - PIXY_Y_CENTER

      # Apply corrections to the pan/tilt gimbals with the goal #
      # of putting the target in the center of Pixy's focus.    #
      pan_gimbal.update(pan_error)
      tilt_gimbal.update(tilt_error)
      
      set_position_result = pixy_rcs_set_position(PIXY_RCS_PAN_CHANNEL, pan_gimbal.position)

      if set_position_result < 0:
        print 'Error: pixy_rcs_set_position() [%d] ' % result
        pixy_error(result)
	board.exit()
        sys.exit(2)

      set_position_result = pixy_rcs_set_position(PIXY_RCS_TILT_CHANNEL, tilt_gimbal.position)

      if set_position_result < 0:
        print 'Error: pixy_rcs_set_position() [%d] ' % result
        pixy_error(result)
	board.exit()
        sys.exit(2)

    if (frame_index % 2) == 0:
      if count == 1:
        if (block.signature == 4):
	 m2 = interp1d([-160,160],[-3,3])
         delta_pan=m2(pan_error)
         if (delta_pan > 0.2 or delta_pan < -0.2):
	  current_based=current_based+delta_pan
          if (current_based > 180):
           current_based=180
          if (current_based < 0):
           current_based=0
          pin12.write(current_based)

         swap=random.randint(0,3)
	 if (swap == 0):
	  m = interp1d([-100,100],[-3,3])
          print "up1 = %d , m = %f"%(tilt_error,m(tilt_error))
          current_tilt=current_tilt-m(tilt_error)
          if (current_tilt > 90):
           current_tilt=90
          if (current_tilt < 0):
           current_tilt=0
          pin_tilt.write(current_tilt)
         elif (swap == 1):
	  m = interp1d([-100,100],[-5,5])
          delta_tilt=m(tilt_error)
          current_rz=current_rz-delta_tilt
          if (current_rz > 160):
           current_rz=160
          if (current_rz < 30):
           current_rz=30
          pin10.write(current_rz)
          print "current_rz = %d , m = %f"%(tilt_error,delta_tilt)
         else:
          m = interp1d([-100,100],[-5,5])
          current_z=current_z-m(tilt_error)
          if (current_z > 120):
           current_z=120
          if (current_z < 30):
           current_z=30
          pin11.write(current_z)
          print "current_z = %d , m = %f"%(tilt_error,m(tilt_error))

    frame_index = frame_index + 1

  pixy_close()

if __name__ == "__main__":
  main()

# LEIMON 2015 #
