Welcome to the uarm wiki!
## Install Raspbian
Download https://www.raspberrypi.org/downloads/raspbian/. แนะนำใช้ RASPBIAN WHEEZY ไม่พบปัญหาเขียนไฟล์ผิดพลาด

หลังติดตั้ง ใช้ raspi-config ขยายพื้นที่จาก 4GB เป็นเต็มขนาดของ SDCard

## System Overview
ใช้ Raspberry PI2 Model B(RPi) ควบคุม uARM ผ่านทางเฟรมเวิร์ค firmata
มี USB joystick แบบใช้ในเกมส์ Play Station 2 เชื่อม RPi
## Install dependence
```bash
sudo apt-get install git cmake python-pyfirmata
```

* python
```python
sudo pip install pyfirmata
sudo pip install pygames
```

## udev
* /etc/udev/rules.d/50-UARM.rules
```bash
SUBSYSTEM=="tty", ATTRS{idVendor}=="0403", ATTRS{idProduct}=="6001", ATTRS{serial}=="A600CRGK", SYMLINK+="ttyUARM"
SUBSYSTEM=="tty", ATTRS{idVendor}=="0403", ATTRS{idProduct}=="6001", ATTRS{serial}=="A600CQZ6", SYMLINK+="ttyUARM_W"
```

## Firmata uARM PIN

use StandardFirmataEEPROM for Uarm board.

'x:y:z' <br>
x = Digital(d), Analog(a) <br>
y = PIN number (0..N) <br>
z = Output(o), Input(i), PWM(p), Servo(s) <br>

ตัวอย่าง

```python
pin10 = board.get_pin('d:10:s')
```

| Sheild PIN   |      Description     | Code Mapping |
|----------|:-------------:| ---------:|
| 2 |  Suction's sensor | d:2:i|
| 3 |  Beep sound   | d:3:o|
| 4 |  | |
| 5 | Release suction |d:5:o|
| 6 | Enable suction | d:6:o|
| 7 |  ||
| 8 | LED  ||
| 9 | Server Gripper ||
| 10 | End servo(4) |d:10:s|
| 11 | Right Server(1) |d:11:s|
| 12 | Based Servo(3) |d:12:s|
| 13 | Left Servo(2) |d:13:s|
